<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('barcode', 21)->nullable();
            $table->integer('catagory');
            $table->integer('brand')->nullable();
            $table->integer('stock');
            $table->decimal('price', 13, 2);
            $table->decimal('wholesalediscount', 5, 2)->nullable();
            $table->decimal('buyin', 13, 2)->nullable();
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
