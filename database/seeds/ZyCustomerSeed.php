<?php

use Illuminate\Database\Seeder;

class ZyCustomerSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zy_customers')->insert([
            ['id' => '1', 'type' => 'Normal'],
            ['id' => '2', 'type' => 'Wholesale']
        ]);
    }
}
