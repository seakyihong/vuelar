<?php

use Illuminate\Database\Seeder;

class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            ['id' => '1', 'username' => 'admin', 'position' => '1', 'password' => Hash::make('admin2k18'), 'created_at' => '2018-06-29 08:48:34', 'updated_at' => '2018-06-29 08:48:34']
        ]);
    }
}
