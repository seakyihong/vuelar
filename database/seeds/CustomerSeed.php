<?php

use Illuminate\Database\Seeder;

class CustomerSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            ['id' => 1,
            'name' => 'Walk-In',
            'gender' => 3,
            'type' => 1,
            'created_by' => 1,
            'created_at' => '2018-06-29 08:48:34',
            'updated_at' => '2018-06-29 08:48:34'],
            ['id' => 2,
            'name' => 'Wholesale',
            'gender' => 3,
            'type' => 2,
            'created_by' => 1,
            'created_at' => '2018-06-29 08:48:34',
            'updated_at' => '2018-06-29 08:48:34']
        ]);
    }
}
