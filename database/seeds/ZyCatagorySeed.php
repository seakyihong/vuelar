<?php

use Illuminate\Database\Seeder;

class ZyCatagorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zy_catagories')->insert([
            'id' => '1',
            'catagory' => 'Other',
        ]);
    }
}
