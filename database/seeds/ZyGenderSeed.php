<?php

use Illuminate\Database\Seeder;

class ZyGenderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zy_genders')->insert([
            ['id' => '1', 'gender' => 'Male'],
            ['id' => '2', 'gender' => 'Female'],
            ['id' => '3', 'gender' => 'Other']
        ]);
    }
}
