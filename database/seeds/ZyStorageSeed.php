<?php

use Illuminate\Database\Seeder;

class ZyStorageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zy_storages')->insert([
            ['id' => '1', 'type' => 'Warehouse'],
            ['id' => '2', 'type' => 'Store']
        ]);
    }
}
