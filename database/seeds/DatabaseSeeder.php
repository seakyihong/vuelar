<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeed::class,
            CustomerSeed::class,
            ZyBrandSeed::class,
            ZyCatagorySeed::class,
            ZyCustomerSeed::class,
            ZyGenderSeed::class,
            ZyStorageSeed::class,
            ZyPaidSeed::class,
        ]);
    }
}
