<?php

use Illuminate\Database\Seeder;

class ZyBrandSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zy_brands')->insert([
            'id' => '1',
            'brand' => 'Other',
        ]);
    }
}
