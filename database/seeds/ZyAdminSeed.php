<?php

use Illuminate\Database\Seeder;

class ZyAdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zy_admins')->insert([
            ['id' => '1', 'position' => 'Master'],
            ['id' => '2', 'position' => 'Admin'],
            ['id' => '3', 'position' => 'Seller'],
        ]);
    }
}
