<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap3.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
    <link rel="icon" href="{{asset('img/admin.ico')}}">
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
</head>
<body>
<div class="container">
    <div id="login">
        <img src="/img/loginlogo.png">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                            {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">
                            Username</label>
                        <div class="col-sm-9 no-gutters">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus autocomplete="off">                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">
                            Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9 rme">
                            <div class="checkbox">
                                <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    Remember me
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group last loginbutton">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" name="submit" class="btn btn-info btn-lg">
                                <span>Login</span></button>
                        </div>
                    </div>
					</form>
                </div>
    </div>

</div>
</body>
</html>
