export const routes = [
    {
        path: '/admin',
        component: require('./components/Admin/Index.vue'),
        children: [

            { path: 'products', component: require('./components/admin/products/Index.vue') },
            { path: 'products/create', component: require('./components/admin/products/Create.vue') },
            
            { path: 'storages', component: require('./components/admin/storages/Index.vue') },
            { path: 'storages/create', component: require('./components/admin/storages/Create.vue') },



            { path: 'customers', component: require('./components/admin/customers/Index.vue') },
            { path: 'customers/create', component: require('./components/admin/customers/Create.vue') },

            { path: '*', component: require('./components/NotFound.vue') }
        ]
    }
];