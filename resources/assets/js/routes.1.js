export const routes = [
    {
        path: '/admin',
        component: require('./components/Admin/Index.vue'),
        children: [
            { path: 'dashboard', component: require('./components/admin/Dashboard.vue') },
            { path: 'pos', component: require('./components/admin/pos/Pos.vue') },

            { path: 'products', component: require('./components/admin/products/Index.vue') },
            { path: 'products/create', component: require('./components/admin/products/Create.vue') },
            
            { path: 'storages', component: require('./components/admin/storages/Index.vue') },
            { path: 'storages/create', component: require('./components/admin/storages/Create.vue') },

            { path: 'inventories', component: require('./components/admin/inventories/Index.vue') },

            { path: 'orders', component: require('./components/admin/orders/Main.vue'), children: [
                { path: '/', component: require('./components/admin/orders/Index.vue') },
                { path: 'pending', component: require('./components/admin/orders/Pending.vue') },
                { path: 'history', component: require('./components/admin/orders/History.vue') },
                { path: 'deleted', component: require('./components/admin/orders/Deleted.vue') },
            ]},
            { path: 'reports', component: require('./components/admin/reports/Main.vue'), children: [
                { path: '', component: require('./components/admin/reports/Index.vue') },
            ]},
            { path: 'analyses', component: require('./components/admin/analyses/Main.vue'), children: [
                { path: '', component: require('./components/admin/analyses/Index.vue') },
            ]},
            { path: 'orders/:id', component: require('./components/admin/orders/Action.vue') },

            { path: 'transactions', component: require('./components/admin/transactions/Main.vue'), children: [
                { path: '/', component: require('./components/admin/transactions/Index.vue') },
                { path: 'pending', component: require('./components/admin/transactions/Pending.vue') },
                { path: 'history', component: require('./components/admin/transactions/History.vue') },
                { path: 'deleted', component: require('./components/admin/transactions/Deleted.vue') },
            ]},
            { path: 'sales', component: require('./components/admin/sales/Main.vue'), children: [
                { path: '/', component: require('./components/admin/sales/Index.vue') },
                { path: 'history', component: require('./components/admin/sales/History.vue') },
                { path: 'refund', component: require('./components/admin/sales/Refund.vue') },
            ]},
            { path: 'transactions/:id', component: require('./components/admin/transactions/Action.vue') },
            
            { path: 'bundles', component: require('./components/admin/bundles/Main.vue'), children: [
                { path: '/', component: require('./components/admin/bundles/Index.vue') },
                { path: 'create', component: require('./components/admin/bundles/Create.vue') },
            ]},

            { path: 'customers', component: require('./components/admin/customers/Index.vue') },
            { path: 'customers/create', component: require('./components/admin/customers/Create.vue') },

            { path: 'accounts', component: require('./components/admin/accounts/Index.vue') },
            { path: 'accounts/create', component: require('./components/admin/accounts/Create.vue') },


            { path: 'profile', component: require('./components/admin/Profile.vue') },
            { path: '*', component: require('./components/NotFound.vue') }
        ]
    }
];