
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

import {routes} from './routes';
import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);


import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.toast = toast;


window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.component('pagination', require('laravel-vue-pagination'))

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: '#38c172',
    failedColor: 'red',
    thickness: '6px',
    height: '3px'
  })

  import 'chart.js';
  import 'hchs-vue-charts';
  
  Vue.use(window.VueCharts);
  import 'countup.js';
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

  import Vue from 'vue';
  import VueHtmlToPaper from 'vue-html-to-paper';
  
  const options = {
    name: '',
    specs: [
    ],
    styles: [
      'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
      'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
  }
  
  Vue.use(VueHtmlToPaper, options);

Vue.filter('round', function(n){
    n = parseFloat(n).toFixed(2);
    return n;
});
Vue.filter('sDate',function(created){
    return moment(created).format('D MMM YYYY // hh:mm:ss a');
});
Vue.filter('dDate',function(created){
    return moment(created).format('D MMMM YYYY');
});
Vue.filter('rDate',function(created){
    return moment(created).format('D MMMM YYYY hh:mm:ss a');
});

window.Fire =  new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader.vue'));
Vue.component('scale-loader', require('vue-spinner/src/ScaleLoader.vue'));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue')
);


Vue.component('example-component', require('./components/ExampleComponent.vue'));

const admin = new Vue({
    el: '#admin',
    router,
    data:{
        search: ''
    },
    methods:{
        searchmodel: _.debounce(() =>{
            Fire.$emit('searching');
        }, 400),
    }
});
