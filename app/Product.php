<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    public $primaryKey = 'id';

    public function inventories()
    {
        return $this->hasMany('App\Inventory', 'product', 'id')->with('storage');
    }
}
