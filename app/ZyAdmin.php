<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZyAdmin extends Model
{
    protected $table = "zy_admins";
    public $primaryKey = "id";
    public $timestamps = false;
}
