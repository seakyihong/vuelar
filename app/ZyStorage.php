<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZyStorage extends Model
{
    protected $table = "zy_storages";
    public $primaryKey = "id";
    public $timestamps = false;
}
