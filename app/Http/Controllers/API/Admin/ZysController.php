<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ZyCatagory;
use App\ZyBrand;
use App\ZyStorage;
use App\ZyCustomer;
use App\ZyGender;
use App\ZyAdmin;


class ZysController extends Controller
{
    public function zy_storage_types(){
        return ZyStorage::all();
    }
    public function catagories(){
        return ZyCatagory::all();
    }
    public function brands(){
        return ZyBrand::all();
    }
    public function customers(){
        return ZyCustomer::all();
    }
    public function genders(){
        return ZyGender::all();
    }
    public function admins(){
        return ZyAdmin::all();
    }
}
