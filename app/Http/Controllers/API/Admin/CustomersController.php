<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use Auth;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        return Customer::with('type')->with('gender')->with('creator')->latest()->paginate(10);
    }
    public function search(){
        if ($search = \Request::get('q')) {
            $customer = Customer::where(function($query) use ($search){
                $query->where('id','LIKE',"%$search%")
                    ->orWhere('name','LIKE',"%$search%")
                    ->orWhere('email','LIKE',"%$search%")
                    ->orWhere('phone','LIKE',"%$search%");
                    // ->orWhereHas('type', function($query) use ($search){
                    //     $query->where('type','LIKE',"%$search%");
                    // });
            })->with('type')->with('gender')->with('creator')->paginate(10);   
        }else{
            $customer = Customer::with('type')->with('gender')->with('creator')->latest()->paginate(10);
        }
        return $customer;

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'unique:customers|required|max:191',
        ]);
    
            $customer = new Customer;
            $customer->name = $request->input('name');
        if($request->input('gender') == ""){
            $customer->gender = 1;
        }else{
            $customer->gender =  $request->input('gender');
        }
        if($request->input('type') == ""){
            $customer->type = 1;
        }else{
            $customer->type =  $request->input('type');
        }
            $customer->phone =  $request->input('phone');
            $customer->email =  $request->input('email');
            $customer->address =  $request->input('address');
            $customer->created_by =  Auth::user()->id;
    
            $customer->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getType($id)
    {
        return Customer::findOrFail($id)->type;
    }

    public function show($id)
    {
        return Customer::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cus.name' => 'required|max:191|',
            'cus.phone' => 'required|max:191|unique:customers,phone,'. $id .'',
        ]);
    
            $customer = Customer::find($id);
            $customer->name = $request->input('cus.name');
        if($request->input('cus.gender') == ""){
            $customer->gender = 1;
        }else{
            $customer->gender =  $request->input('cus.gender');
        }
        if($request->input('cus.type') == ""){
            $customer->type = 1;
        }else{
            $customer->type =  $request->input('cus.type');
        }
            $customer->phone =  $request->input('cus.phone');
            $customer->email =  $request->input('cus.email');
            $customer->address =  $request->input('cus.address');
            $customer->created_by =  Auth::user()->id;
    
            $customer->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
