<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Storage;
use Auth;

class StoragesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Storage::with('type')->latest()->paginate(10);
    }

    public function search(){
        if ($search = \Request::get('q')) {
            $storages = Storage::with('type')->where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                    ->orWhere('id','LIKE',"%$search%")
                    ->orWhere('address','LIKE',"%$search%")
                    ->orWhere('phone1','LIKE',"%$search%")
                    ->orWhereHas('type', function($query) use ($search){
                        $query->where('type','LIKE',"%$search%");
                    });
            })->paginate(10);   
        }else{
            $storages = Storage::with('type')->paginate(10);
        }
        return $storages;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $this->validate($request, [
        'name' => 'unique:storages|required|max:191',
        'type' => 'required',
        'phone' => 'required|max:191',
        'address' => 'required',
    ]);


        $storage = new Storage;
        $storage->name = $request->input('name');
        $storage->phone1 =  $request->input('phone');
        $storage->type =  $request->input('type');
        $storage->address =  $request->input('address');

        $storage->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
