<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Admin;
use App\AdminDetail;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth:admin-api');
    }

    public function index()
    {
        return Admin::with('detail')->with('position')->latest()->paginate(10);
    }
    public function workplace()
    {
        return Auth::user()->workplace;
    }
    public function changeworkplace(Request $request)
    {
         Auth::user()->workplace = $request->storage;
         Auth::user()->save();
    }
    public function search(){
        if ($search = \Request::get('q')) {
            $accounts = Admin::with('detail')->where(function($query) use ($search){
                $query->where('id','LIKE',"%$search%")
                    ->orWhere('username','LIKE',"%$search%")
                    ->orWhereHas('detail', function($query) use ($search){
                        $query->where('fullname','LIKE',"%$search%")
                        ->orWhere('fullname','LIKE',"%$search%")
                        ->orWhere('phone1','LIKE',"%$search%")
                        ->orWhere('email','LIKE',"%$search%");
                    });
            })->paginate(10);   
        }else{
            $accounts = Admin::with('detail')->latest()->paginate(10);
        }
        return $accounts;

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
        ]);
    
            $account = new Admin;
            $accd = new AdminDetail;
            $account->username = $request->input('username');
            $account->password = Hash::make($request->input('password'));
            $accd->fullname = $request->input('name');
        if($request->input('gender') == ""){
            $accd->gender = 1;
        }else{
            $accd->gender =  $request->input('gender');
        }
        if($request->input('position') == ""){
            $account->position = 1;
        }else{
            $account->position =  $request->input('position');
        }
            $accd->phone1 =  $request->input('phone');
            $accd->email =  $request->input('email');
            $accd->address =  $request->input('address');
            $accd->created_by =  Auth::user()->id;

            $accd->save();
            $account->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Admin::with('detail')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $account = Admin::find($id);
            $accd = AdminDetail::find($id);
            $account->username = $request->input('acc.username');
            $account->password = Hash::make($request->input('acc.password'));
            $accd->fullname = $request->input('acc.detail.fullname');
        if($request->input('acc.gender') == ""){
            $accd->gender = 1;
        }else{
            $accd->gender =  $request->input('acc.detail.gender');
        }
        if($request->input('acc.position') == ""){
            $account->position = 1;
        }else{
            $account->position =  $request->input('acc.position');
        }
            $accd->phone1 =  $request->input('acc.detail.phone');
            $accd->email =  $request->input('acc.detail.email');
            $accd->address =  $request->input('acc.detail.address');
            $accd->created_by =  Auth::user()->id;

            $accd->save();
            $account->save();
            return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
