<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ZyCatagory;
use App\ZyBrand;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }
    
    public function index()
    {
        return Product::latest()->paginate(10);
    }
    


    public function search(){
        if ($search = \Request::get('q')) {
            $products = Product::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                    ->orWhere('id','LIKE',"%$search%")
                    ->orWhere('barcode','LIKE',"%$search%")
                    ->orWhere('price','LIKE',"%$search%")
                    ->orWhere('buyin','LIKE',"%$search%")
                    ->orWhere('stock','LIKE',"%$search%");
            })->paginate(10);
        }else{
            $products = Product::latest()->paginate(10);
        }
        return $products;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $this->validate($request, [ 
        'name' => 'unique:products|required|max:191',
        'price' => 'required|numeric|min:0',
        'buyin' => 'nullable|numeric|min:0',
        'wholesalediscount' => 'nullable|numeric|between:0,100.00',
        'thumnail' => 'image|nullable|max:1999',
    ]);
        if($request->hasFile('thumnail')){
            $filenameWithExt = $request->file('thumnail')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('thumnail')->getClientOriginalExtension();
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            $path = $request->file('thumnail')->storeAs('public/products', $fileNameToStore);
        }else{
            $fileNameToStore = 'noimage.jpg';
        }
        if($request->input('addCatagory') == 1 || $request->input('addBrand') == 1){
            $this->validate($request, [
                'catagory' => 'unique:zy_catagories',
                'brand' => 'unique:zy_brands',
            ]);
        }
        $product = new Product;
        $product->name = $request->input('name');
        $product->barcode =  $request->input('stock');
        $product->stock =  0;
        $product->active =  1;
    if($request->input('catagory') == ""){
        $product->catagory = 1;
    }elseif($request->input('addCatagory') == 1){
        if(!isset($errors)){
        $catagory = new ZyCatagory;
        $catagory->catagory = $request->input('catagory');
        $catagory->save();
        $product->catagory =  $catagory->id;
        }
    }else{
        $product->catagory =  $request->input('catagory');
    }

    if($request->input('brand') == ""){
        $product->brand = 1;
    }elseif($request->input('addBrand') == 1){

        if(!isset($errors)){
        $brand = new ZyBrand;
        $brand->brand = $request->input('brand');
        $brand->save();
        $product->brand =  $brand->id;
        }
    }else{
        $product->brand =  $request->input('brand');
    }
        $product->price =  $request->input('price');
    if($request->input('wholesalediscount') != ""){
        $product->wholesalediscount =  $request->input('wholesalediscount');
    } 
    if($request->input('buyin') != ""){
        $product->buyin =  $request->input('buyin');
    }     

        $product->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::find($id);
    }
    public function getWholesaleDc($id){
        return Product::findOrFail($id)->wholesalediscount;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findorFail($id);
        if($request->input('prod.sw') == 1){
            $product->active = $request->input('prod.active');
            $product->save();
        }else{
        $this->validate($request, [
            'prod.name' => 'required|max:191|unique:products,name,'. $id .'',
            'prod.price' => 'required|numeric|min: 0',
            'prod.wholesalediscount' => 'nullable|numeric|between:0,100.00',
            'prod.thumnail' => 'image|nullable|max:1999'
        ]);
        $product->name = $request->input('prod.name');
        $product->price = $request->input('prod.price');
        $product->wholesalediscount = $request->input('prod.wholesalediscount');
        $product->barcode = $request->input('prod.barcode');
        $product->catagory = $request->input('prod.catagory');
        $product->buyin = $request->input('prod.buyin');
        $product->save();
        return 1;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
    }
}
