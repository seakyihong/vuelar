<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Storage;

class ZxsController extends Controller
{
    public function zx_storages(){
        return Storage::all();
    }
}
