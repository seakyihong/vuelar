<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $table = 'storages';
    public $primaryKey = 'id';

    public function type(){
        return $this->belongsTo('App\ZyStorage', 'type', 'id');
    }
}
