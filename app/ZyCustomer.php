<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZyCustomer extends Model
{
    protected $table = "zy_customers";
    public $primaryKey = "id";
    public $timestamps = false;
}
