<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZyBrand extends Model
{
    protected $table = "zy_brands";
    public $primaryKey = "id";
    public $timestamps = false;
}
