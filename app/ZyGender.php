<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZyGender extends Model
{
    protected $table = "zy_genders";
    public $primaryKey = "id";
    public $timestamps = false;
}
