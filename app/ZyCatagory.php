<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZyCatagory extends Model
{
    protected $table = "zy_catagories";
    public $primaryKey = "id";
    public $timestamps = false;
}
