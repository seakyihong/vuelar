<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminDetail extends Model
{
    protected $table = 'admin_details';
    public $primaryKey = 'id';
    public $timestamps = false;

    public function gender(){
        return $this->belongsTo('App\ZyGender', 'gender', 'id');
    }
    
    public function creator(){
        return $this->belongsTo('App\Admin', 'created_by', 'id');
    }
}
