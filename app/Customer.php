<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    public $primaryKey = 'id';

    public function type(){
        return $this->belongsTo('App\ZyCustomer', 'type', 'id');
    }
    public function gender(){
        return $this->belongsTo('App\ZyGender', 'gender', 'id');
    }
    public function creator(){
        return $this->belongsTo('App\Admin', 'created_by', 'id');
    }
}
