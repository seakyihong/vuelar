<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'position', 'fullname', 'phonenumber'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // public function getPosition()
    // {
    //     return $this->belongsTo('App\AccountPosition', 'position', 'id');
    // }
    // public function detail()
    // {
    //     return $this->belongsTo('App\AdminDetail', 'id', 'id');
    // }

    public function detail(){
        return $this->belongsTo('App\AdminDetail', 'id', 'id')->with('gender')->with('creator');
    }

    public function position(){
        return $this->belongsTo('App\ZyAdmin', 'position', 'id');
    }
}
