<?php

use Illuminate\Http\Request;

/*php
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('admin/logout', 'Auth\Admin\LoginController@logout');
Route::get('profile', 'API\UserController@profile');
Route::apiResources(['user' => 'API\UserController']);

Route::apiResources(['products' => 'API\Page\ProductsController']);

Route::apiResources(['admin/products' => 'API\Admin\ProductsController']);
Route::apiResources(['admin/storages' => 'API\Admin\StoragesController']);
Route::apiResources(['admin/inventories' => 'API\Admin\InventoriesController']);
Route::apiResources(['admin/orders' => 'API\Admin\OrdersController']);
Route::apiResources(['admin/customers' => 'API\Admin\CustomersController']);
Route::apiResources(['admin/accounts' => 'API\Admin\AccountsController']);
Route::apiResources(['admin/transactions' => 'API\Admin\TransactionsController']);
Route::apiResources(['admin/checkout' => 'API\Admin\CheckoutsController']);
Route::apiResources(['admin/bundles' => 'API\Admin\BundlesController']);

Route::get('admin/workplace', 'API\Admin\AccountsController@workplace');
Route::get('admin/order/pending', 'API\Admin\OrdersController@pending');
Route::get('admin/order/history', 'API\Admin\OrdersController@history');
Route::get('admin/order/deleted', 'API\Admin\OrdersController@deleted');
Route::get('admin/transaction/history', 'API\Admin\TransactionsController@history');
Route::get('admin/transaction/deleted', 'API\Admin\TransactionsController@deleted');

Route::get('admin/top10sales', 'API\Admin\AnalysesController@top10sales');

Route::get('admin/analysis/invoices', 'API\Admin\AnalysesController@invoices');

Route::get('admin/sales', 'API\Admin\SalesController@index');

Route::get('admin/inspectreceipt/{id}', 'API\Admin\SalesController@inspectReceipt');

Route::get('admin/product/pos', 'API\Admin\ProductsController@pos');
Route::get('admin/storage/search', 'API\Admin\StoragesController@search');
Route::get('admin/sale/search', 'API\Admin\SalesController@search');
Route::get('admin/inventory/search', 'API\Admin\InventoriesController@search');
Route::get('admin/customer/search', 'API\Admin\CustomersController@search');
Route::get('admin/order/search', 'API\Admin\OrdersController@search');
Route::get('admin/transaction/search', 'API\Admin\TransactionsController@search');
Route::get('admin/account/search', 'API\Admin\AccountsController@search');
Route::get('admin/report/sales', 'API\Admin\ReportsController@sales');

Route::get('admin/zx/storages', 'API\Admin\ZxsController@zx_storages');

Route::get('admin/zy/storage_types', 'API\Admin\ZysController@zy_storage_types');
Route::get('admin/zy/catagories', 'API\Admin\ZysController@catagories');
Route::get('admin/zy/brands', 'API\Admin\ZysController@brands');
Route::get('admin/zy/customers', 'API\Admin\ZysController@customers');
Route::get('admin/zy/genders', 'API\Admin\ZysController@genders');
Route::get('admin/zy/admins', 'API\Admin\ZysController@admins');

Route::post('admin/changeworkplace', 'API\Admin\AccountsController@changeworkplace');

Route::get('admin/get/productWholesaleDc/{id}', 'API\Admin\ProductsController@getWholesaleDc');
Route::get('admin/get/customerType/{id}', 'API\Admin\CustomersController@getType');

Route::get('admin/check/inventories', 'API\Admin\ConfirmsController@check_inventories');

Route::put('admin/pending/orders/{id}', 'API\Admin\ConfirmsController@pending_orders');
Route::put('admin/cc/orders/{id}', 'API\Admin\ConfirmsController@orders');
Route::put('admin/cc/transactions/{id}', 'API\Admin\ConfirmsController@transactions');

Route::put('admin/rr/orders/{id}', 'API\Admin\RemoveRecordsController@orders');
Route::put('admin/rr/transactions/{id}', 'API\Admin\RemoveRecordsController@transactions');

Route::put('profile', 'API\UserController@updateProfile');

