<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('admin')->group(function(){
    Route::get('/', function() {return redirect('/admin/dashboard');});
    Route::get('/login', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\Admin\LoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\Admin\LoginController@logout')->name('admin.logout');

    Route::get('/{path}',"AdminsController@index")->where( 'path', '([A-z\d-\/_.]+)?' );
});


Route::get('{any}',
    function () {return view('home');
})->where('any', '.*');